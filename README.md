# README #

This README would normally document whatever steps are necessary to get your application up and running.

Version : 0.2

Link : http://1.maouquizz2.appspot.com/

Fonctionnement du site :

Session : la création d'une session permet d'envoyer des questions aux utilisateurs joignant cette session et de récolter les réponses données. Le créateur de la session peut alors créer des questions soit à choix multiple soit avec une réponse correcte soit une question ouverte. Le créteur peut donner un temps limité aux utilisateurs pour donner une réponse, il peut également afficher les réponses données par les utilisateurs à chaque question de la session.

Page Home : elle permet de créer une session lorsque l'utilisateur est connecté ou de créer un compte ou de se connecter si l'utilisateur ne l'est pas

Page Join : elle permet de rejoindre une session active afin de pouvoir répondre aux questions. L'utilisateur est alors mené à une page d'attente, la page s'actualise lors de l'envoie d'une question et il peut alors répondre à cette question.

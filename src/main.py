#!/usr/bin/env python
# encoding=utf8  
import sys  

reload(sys)  
sys.setdefaultencoding('utf8')

from handlers.Helpers import *
from handlers.MainHandler import *
from handlers.User import *
from handlers.Session import *
from handlers.SessionHandler import *
from handlers.SessionUserHandler import *
from handlers.UserHandler import *
from handlers.QuestionHandler import *

class MainPage(Handler):
    def get(self):
        self.render("main_page.html")

app = webapp2.WSGIApplication([
    ('/', MainPage),
    (r'/login/?', Login),
    (r'/logout/?', Logout),
    (r'/signup/?', CreateUser),
    (r'/session/?', SessionHandler),
    (r'/newquestion/?', CreateQuestion),
    (r'/newsession/?', CreateSession),
    (r'/join_session/?', JoinHandler),
    (r'/end/?', EndSessionHandler),
    (r'/send/?', QuestionDispatcher),
    (r'/stop/?', EndQuestionHandler),
    (r'/send_answer/?', QuestionManifold),
    (r'/sendusr/?', QuestionSender),
    (r'/quit/?', QuitSessionHandler)
], debug=True)
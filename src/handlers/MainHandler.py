#!/usr/bin/env python
# encoding=utf8  
import sys 
reload(sys)  
sys.setdefaultencoding('utf8')


import os
import re
import sys
import webapp2
import jinja2

from handlers.Helpers import *
from handlers.User import User
from google.appengine.api import channel
from handlers.Session import *

template_dir = os.path.join(os.path.dirname(__file__), '../html')
jinja_env = jinja2.Environment(loader = jinja2.FileSystemLoader(template_dir), autoescape=True)

class Handler(webapp2.RequestHandler):
    def write(self, *a, **kw):
    	self.response.write(*a, **kw)

    def render_str(self, template, **params):
		t = jinja_env.get_template(template)
		return t.render(params)

    def render(self, template, **kw):
		cookie = self.request.cookies.get('user_id')
		user = None
		if cookie:
			secure_val = check_secure_val(cookie)
			if secure_val:
				id = int(secure_val)
				user = User.get_by_id(id)

		if user:
			kw["user"] = user
			#On crée le token à envoyer au client si celui-ci est connecté. Il sera toujours accessible
			strid = str(user.key().id())
			token = None
			if strid in tokens:
				token = tokens[strid]
			else:
				token = channel.create_channel(strid)
				tokens[strid] = token

			kw["token"] = token

			#On regarde si le user a une session active
			found = False
			for id_sess, list_users in SESSION_USERS.iteritems():
				if strid in list_users:
					found = True
					id_session = id_sess
					break

			if found == True:
				#On récupère l'objet session
				session = None
				session = Session.get_by_id(int(id_session))
				kw["session"] = session

		name_page = ''
		if template == "join_session.html":
			name_page = 'Joindre une session'
		elif template == "login.html":
			name_page = 'Login'
		elif template == "main_page.html":
			name_page = 'Accueil'
		elif template == 'session.html':
			name_page = 'Session maître'
		elif template == 'session_user.html':
			name_page = 'Session utilisateur'
		elif template == 'signup.html':
			name_page = 'Inscription'

		kw["name_page"] = name_page

		self.write(self.render_str(template, **kw))
#!/usr/bin/env python
# encoding=utf8  
import sys  

reload(sys)  
sys.setdefaultencoding('utf8')

import os
import webapp2
import jinja2
import hmac
import hashlib
import random
import string
import logging
import time
import json

from User import *
from Session import *
from Helpers import *
from google.appengine.api import channel
from MainHandler import Handler

class Chat(Handler):
	def post(self):
		#On récupère l'id du user
		cookie = self.request.cookies.get('user_id')
		user = None
		if cookie:
			secure_val = check_secure_val(cookie)
			if secure_val:
				id = int(secure_val)
				user = User.get_by_id(id)

		if user:

			#On récupère la session courante de l'utilisateur connecté
			id_session = -1
			strid = str(user.key().id())
			for id_sess, list_users in SESSION_USERS.iteritems():
				if strid in list_users:
					id_session = id_sess
					break

			if id_session == -1:
				return

			#On récupère le message de l'utilisateur
			message = 'chat|' + user.username + '|' + self.request.get("message");

			#On envoie le message à tout le monde
			for i in range(1, len(SESSION_USERS[id_session])):
				id_user = SESSION_USERS[id_session][i]
				channel.send_message(id_user, message)
#!/usr/bin/env python
# encoding=utf8  
import sys  

reload(sys)  
sys.setdefaultencoding('utf8')

import os
import webapp2
import jinja2
import hmac
import hashlib
import random
import string
import logging

from google.appengine.ext import db

class Question(db.Model):
	type_question = db.IntegerProperty(required = True) #1 = question ouverte, 2 = question directe, 3 = QCM
	question = db.StringProperty(required = True)
	answers_corrects = db.TextProperty(required = True) #Pour les questions directes et pour les QCMs
	answers_wrongs = db.TextProperty(required = True) #Pour les QCMs
	id_session = db.IntegerProperty(required = True)
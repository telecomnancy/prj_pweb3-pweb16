#!/usr/bin/env python
# encoding=utf8  
import sys  
reload(sys)  
sys.setdefaultencoding('utf8')

import os
import webapp2
import jinja2
import hmac
import hashlib
import random
import string
import logging

from Helpers import *
from MainHandler import Handler
from User import User
from Session import Session
from google.appengine.ext import db
from google.appengine.api import channel

template_dir = os.path.join(os.path.dirname(__file__), 'html')
jinja_env = jinja2.Environment(loader = jinja2.FileSystemLoader(template_dir), autoescape=True)

class SessionHandler(Handler):
	def render_pageMaster(self, session, questions, nb_users = 0):
		self.render("session.html", session = session, questions = questions, nb_users = nb_users)

	def render_pageUser(self, session):
		self.render("session_user.html", session = session)

	def get(self):
		#On récupère l'utilisateur connecté
		cookie = self.request.cookies.get('user_id')
		user = None
		if cookie:
			secure_val = check_secure_val(cookie)
			if secure_val:
				id = int(secure_val)
				user = User.get_by_id(id)

		if not user:
			self.redirect('/')
			return

		#On récupère la session dans laquelle il se trouve
		id_session = -1
		found = False
		strid = str(user.key().id())
		for id_sess, list_users in SESSION_USERS.iteritems():
			if strid in list_users:
				found = True
				id_session = id_sess
				break

		if found == False:
			self.render("main_page.html")
			return

		#On récupère l'objet session
		session = None
		session = Session.get_by_id(int(id_session))

		questions = db.GqlQuery("SELECT * FROM Question WHERE id_session = :1", session.key().id())
		questions = list(questions)

		#on regarde s'il est master (premier de la liste) ou simple user
		if SESSION_USERS[id_session][0] == strid:
			#Master
			nb_users = len(SESSION_USERS[id_session]) - 1
			self.render_pageMaster(session = session, questions = questions, nb_users = nb_users)
		else:
			#User
			self.render_pageUser(session = session)


class JoinHandler(Handler):
	def render_page(self, sessions = [], session = None, questions = []):
		self.render("join_session.html", sessions = sessions)

	def get(self):

		id_session = self.request.get('id_session')
		cookie = self.request.cookies.get('user_id')
		user = None
		if cookie:
			secure_val = check_secure_val(cookie)
			if secure_val:
				id = int(secure_val)
				user = User.get_by_id(id)

		if user:
			strid = str(user.key().id())

			#On vérifie si le user ets déjà connecté à une session
			found = False
			for id_sess, list_users in SESSION_USERS.iteritems():
				if strid in list_users:
					found = True
					id_session = id_sess
					break

			if found == True:
				self.redirect('/')
				return
			else:
				sessions = []
				for id_sess, list_users in SESSION_USERS.iteritems():
					tmp = Session.get_by_id(int(id_sess))
					sessions.append(tmp)
				self.render_page(sessions = sessions)
		else:
			self.redirect('/')

	def post(self):
		id_session = self.request.get('id_session')
		cookie = self.request.cookies.get('user_id')
		user = None
		if cookie:
			secure_val = check_secure_val(cookie)
			if secure_val:
				id = int(secure_val)
				user = User.get_by_id(id)

		if user:
			strid = str(user.key().id())

			#On supprime l'utilisateur s'il appartient à d'autres sessions
			for id_sess, list_users in SESSION_USERS.iteritems():
				if strid in list_users:
					#Dans ce cas on le supprime de la liste
					index = SESSION_USERS[id_sess].index(strid)
					del SESSION_USERS[id_sess][index]
					break

			#On ajoute l'utilisateur à la session courante
			if id_session in SESSION_USERS.keys():
				if strid not in SESSION_USERS[id_session]:
					SESSION_USERS[id_session].append(strid)
			else:
				SESSION_USERS[id_session] = [strid]

			#On notifie le master de la connexion
			mess = 'nb_users|' + str(len(SESSION_USERS[id_session]) - 1)
			channel.send_message(SESSION_USERS[id_session][0], mess)

			#On redirige le user
			channel.send_message(strid, "/session")

class EndSessionHandler(Handler):
	def get(self):
		#On récupère le user connecté
		cookie = self.request.cookies.get('user_id')
		user = None
		if cookie:
			secure_val = check_secure_val(cookie)
			if secure_val:
				id = int(secure_val)
				user = User.get_by_id(id)

		if not user:
			self.redirect('/')
			return

		#On récupère sa session active
		id_session = -1
		strid = str(user.key().id())
		for id_sess, list_users in SESSION_USERS.iteritems():
			if strid in list_users:
				id_session = id_sess
				break

		if id_session == -1:
			self.redirect('/')
			return

		for i in range(0, len(SESSION_USERS[id_session])):
			id_urs = SESSION_USERS[id_session][i]
			channel.send_message(id_urs, "end")

		del SESSION_USERS[str(id_session)]

		self.redirect('/')

class QuitSessionHandler(Handler):
	def get(self):

		#On récupère le user connecté
		cookie = self.request.cookies.get('user_id')
		user = None
		if cookie:
			secure_val = check_secure_val(cookie)
			if secure_val:
				id = int(secure_val)
				user = User.get_by_id(id)

		if user:
			#On supprime l'utilisateur de la session s'il n'est pas master
			strid = str(user.key().id())
			for id_sess, list_users in SESSION_USERS.iteritems():
				for i in range(1, len(SESSION_USERS[id_sess])):
					if strid == SESSION_USERS[id_sess][i]:
						del SESSION_USERS[id_sess][i]
						mess = 'nb_users|' + str(len(SESSION_USERS[id_sess]) - 1)
						channel.send_message(SESSION_USERS[id_sess][0], mess)
						break

		self.redirect('/')

class CreateSession(Handler):
	def post(self):
		name = self.request.get("session_name")

		#On vérifie que le nom est renseigné
		if name is None or name == '':
			error = 'You must fill in the session name!'
			self.render('main_page.html', error = error)
			return

		#On vérifie qu'il n'a y a pas de session actives avec le même nom
		for id_sess, list_users in SESSION_USERS.iteritems():
			#On récupère la question dans la DB
			sess = Session.get_by_id(int(id_sess))

			if name == sess.name:
				error = 'A session with the same name is currently running!'
				self.render('main_page.html', error = error)
				return

		cookie = self.request.cookies.get('user_id')
		user = None
		if cookie:
			secure_val = check_secure_val(cookie)
			if secure_val:
				id = int(secure_val)
				user = User.get_by_id(id)

		id_usr = 0
		if user:
			id_usr = user.key().id()
		else:
			self.redirect('/')

		session = Session(name = name, creatorID = id_usr)
		session.put()

		id_session = str(session.key().id())

		strid = str(user.key().id())
		if id_session in SESSION_USERS.iteritems():
			if strid not in SESSION_USERS[id_session]:
				SESSION_USERS[id_session].append(strid)
		else:
			SESSION_USERS[id_session] = [strid]

		self.redirect('/session')
#!/usr/bin/env python
# encoding=utf8  
import sys  

reload(sys)  
sys.setdefaultencoding('utf8')

import os
import webapp2
import jinja2
import hmac
import hashlib
import random
import string
import logging

from google.appengine.ext import db

class Session(db.Model):
	name = db.StringProperty(required = True)
	creatorID = db.IntegerProperty(required = True)
#!/usr/bin/env python
# encoding=utf8  
import sys  

reload(sys)  
sys.setdefaultencoding('utf8')

import os
import webapp2
import jinja2
import hmac
import hashlib
import random
import string
import logging
import time
import json

from Question import *
from Session import *
from MainHandler import *
from AnswerUser import *
from google.appengine.api import channel

class QuestionDispatcher(Handler):

	#Fonction appelée en asynchrone
	def post(self):

		#On récupère l'utilisateur
		cookie = self.request.cookies.get('user_id')
		user = None
		if cookie:
			secure_val = check_secure_val(cookie)
			if secure_val:
				id = int(secure_val)
				user = User.get_by_id(id)

		if user:
			#On récupère sa session active du master
			id_session = -1
			strid = str(user.key().id())
			for id_sess, list_users in SESSION_USERS.iteritems():
				if list_users[0] == strid:
					id_session = id_sess
					break

			if id_session == -1:
				return

			#On récupère l'id de la question
			idq = self.request.get("id_question")
			question = self.request.get("question")
			type_question = self.request.get("type_question")
			answers_c = self.request.get_all("ansc")
			answers_w = self.request.get_all("answ")

			data = "question" + "~/" + idq + "|" + type_question + "|" + question

			data += "~/"

			for i in range(0, len(answers_c)):
				data += answers_c[i] if i == len(answers_c) - 1 else answers_c[i] + "|"

			data += "~/"

			for i in range(0, len(answers_w)):
				data += answers_w[i] if i == len(answers_w) - 1 else answers_w[i] + "|"

			channel.send_message(SESSION_USERS[id_session][0], "alert|Your question was sent to all the users connected")
			#On le dispatche à tous les clients (tous les users sauf le master)
			for i in range(1, len(SESSION_USERS[id_session])):
				id_user = SESSION_USERS[id_session][i]
				channel.send_message(id_user, data)

class QuestionManifold(Handler):
	def post(self):
		#On récupère le user connecté
		cookie = self.request.cookies.get('user_id')
		user = None
		if cookie:
			secure_val = check_secure_val(cookie)
			if secure_val:
				id = int(secure_val)
				user = User.get_by_id(id)

		if user:
			#On récupère la session à laquelle participe le user
			id_session = -1
			strid = str(user.key().id())
			for id_sess, list_users in SESSION_USERS.iteritems():
				if strid in list_users:
					id_session = id_sess
					break

			if id_session == -1:
				return

			#On récupère les données de la réponse
			idq = int(self.request.get("id_quest"))
			ans = self.request.get("answer")

			#On ajoute la donnée à la base
			answer_user = AnswerUser(id_user = user.key().id(), id_question = idq, answer = str(ans))
			answer_user.put()

			#On attend un peu
			time.sleep(0.25)

			#On récupère la question
			question = Question.get_by_id(idq);

			#On envoie un message au master
			mess_master = 'answer|' + str(idq) + '|' + ans

			#On envoie la donnée au master => temps réel. Format : "user_name|question|user_answer|right_or_wrong" righ_or_wrong can be true or false
			id_master = SESSION_USERS[id_session][0]

			channel.send_message(id_master, mess_master)

class QuestionSender(Handler):
	def post(self):

		#On récupère le current user
		cookie = self.request.cookies.get('user_id')
		user = None
		if cookie:
			secure_val = check_secure_val(cookie)
			if secure_val:
				id = int(secure_val)
				user = User.get_by_id(id)

		if user:
			strid = str(user.key().id())
			#On récupère la session dont le user est master
			session = -1
			for id_sess, list_users in SESSION_USERS.iteritems():
				if strid in list_users:
					session = int(id_sess)
					break

			if session != -1:

				#On récupère direct le div
				div = 'stats~/' + self.request.get("div")

				#On envoie à tous les users de la session
				for i in range(1, len(SESSION_USERS[str(session)])):
					usr = SESSION_USERS[str(session)][i]
					channel.send_message(usr, div)

class EndQuestionHandler(Handler):
	def post(self):
		#On récupère le user connecté
		cookie = self.request.cookies.get('user_id')
		user = None
		if cookie:
			secure_val = check_secure_val(cookie)
			if secure_val:
				id = int(secure_val)
				user = User.get_by_id(id)

		#Onrécupère sa session master
		if user:
			#On récupère sa session active du master
			id_session = -1
			strid = str(user.key().id())
			for id_sess, list_users in SESSION_USERS.iteritems():
				if list_users[0] == strid:
					id_session = id_sess
					break

			if id_session == -1:
				return

			#On récupère l'id de la question
			id_question = int(self.request.get("id_quest"))

			if id_question == 1:
				return 

			quest = Question.get_by_id(id_question)

			#On envoie le message de fin de question à tout le monde => stop|correct_ans
			mess = 'stop~/' + quest.answers_corrects
			for i in range(1, len(SESSION_USERS[id_session])):
				id_user = SESSION_USERS[id_session][i]
				channel.send_message(id_user, mess) #On indique au client que la question est finie

class CreateQuestion(Handler):
	def render_page(self, sessions = [], session = None, questions = [], nb_users = 0):
		self.render("session.html", sessions = sessions, session = session, questions = questions, nb_users = nb_users)

	def post(self):

		cookie = self.request.cookies.get('user_id')
		user = None
		if cookie:
			secure_val = check_secure_val(cookie)
			if secure_val:
				id = int(secure_val)
				user = User.get_by_id(id)

		#On créer l'objet question
		quest = self.request.get("question")
		answers = self.request.get_all("answer")
		wrongs = self.request.get_all("answer_w")

		type_question = 3 #QCM de base

		if quest == '':
			return

		if len(wrongs) == 0 | ((len(wrongs) == 1 and wrongs[0] == '')):
			if (len(answers) == 1) & (answers[0] == ''):
				type_question = 1 #Si on a rien, c'est une ouverte
			else:
				type_question = 2 #S'il n'y a que des bonnes réponses, alors directe
		elif not (len(wrongs) ==1 and wrongs[0] == ''):
			#il n'y a que des mauvaises réponses
			if not ((len(answers) != 0) and (len(answers) != 1 or answers[0] != '')):
				channel.send_message(str(user.key().id()), 'error|There are only wrong answers!')
				return

		answers_correct = ' '
		if (len(answers) != 0) and (len(answers) != 1 or answers[0] != ''):
			answers_correct = ''
			for i in range(0, len(answers)):
				if i == len(answers) -1:
					answers_correct += answers[i]
				else:
					answers_correct += answers[i] + "|"
 
		answers_w = ' ' 
		if (len(wrongs) != 0) and (len(wrongs) != 1 or wrongs[0] != ''):
			answer_w = ''
			for i in range(0, len(wrongs)):
				if i == len(wrongs) -1:
					answers_w += wrongs[i]
				else:
					answers_w += wrongs[i] + '|'

		#On récupère l'id de la session dont le user est master
		#On parcourt toutes les sessions
		id_session = -1
		strid = str(user.key().id())
		for id_sess, list_users in SESSION_USERS.iteritems():
			if strid in list_users:
				id_session = id_sess
				break

		if id_session == -1:
			self.redirect('/')

		question = Question(type_question = type_question, question = quest, answers_corrects = answers_correct, answers_wrongs = answers_w, id_session = int(id_session))

		#On l'ajoute à la DB
		question.put()

		#On crée la liste des sessions 
		sessions = db.GqlQuery("SELECT * FROM Session")
		sessions = list(sessions)

		#on récupère la session courante
		session = Session.get_by_id(int(id_session))

		#On crée la liste des questions
		#questions = db.GqlQuery("SELECT * FROM Question WHERE id_session = :1", session.key().id())

		#On rend le tout 
		time.sleep(0.25)

		#On récupère le nombre de users
		nb_users = len(SESSION_USERS[id_session]) - 1

		mess = "newquestion|" + str(question.key().id()) + '~/' + str(question.type_question) + '~/' + question.question + '~/' + question.answers_corrects + '~/' + question.answers_wrongs
		channel.send_message(strid, mess)